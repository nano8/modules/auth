<?php

namespace laylatichy\nano\modules\auth;

final class Config {
    public function __construct(
        public readonly string $secretShort,
        public readonly string $secretRefresh,
        public readonly string $issuer,
        public readonly string $audience,
        public readonly string $domain,
        public readonly string $algorithm,
        public readonly int $ttlShort,
        public readonly int $ttlRefresh,
    ) {}
}