<?php

namespace laylatichy\nano\modules\auth\exceptions;

use Exception;
use laylatichy\nano\core\exceptions\IException;
use laylatichy\nano\core\httpcode\HttpCode;
use laylatichy\nano\core\response\Response;

final class UnauthorizedException extends Exception implements IException {
    private HttpCode $httpCode = HttpCode::UNAUTHORIZED;

    /**
     * @param string[] $errors
     */
    public function __construct(private readonly array $errors = ['unauthorized']) {
        parent::__construct('unauthorized', $this->httpCode->code());
    }

    public function response(): Response {
        return useResponse()
            ->withCode($this->httpCode)
            ->withJson([
                'code'     => $this->httpCode->code(),
                'response' => $this->errors,
            ]);
    }
}
