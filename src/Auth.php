<?php

namespace laylatichy\nano\modules\auth;

use DateTime;
use DateTimeZone;
use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use laylatichy\nano\core\request\Request;
use laylatichy\nano\core\response\Response;
use laylatichy\nano\modules\auth\interfaces\JwtSchema;

final class Auth {
    public function __construct(
        private readonly Config $config,
    ) {}

    public function short(Request $request, JwtSchema $schema): JwtSchema {
        try {
            $header = $request->header('Authorization');

            if (!is_string($header)) {
                throw new Exception('missing authorization header');
            }

            $token = str_replace('Bearer ', '', $header);

            $payload = JWT::decode(
                $token,
                new Key($this->config->secretShort, $this->config->algorithm)
            );

            if (!isset($payload->data)) {
                throw new Exception('invalid authorization header');
            }

            return $schema->parse((object)$payload->data);
        } catch (Exception $e) {
            useUnauthorizedException([$e->getMessage()]);

            return $schema;
        }
    }

    public function refresh(Request $request, JwtSchema $schema): JwtSchema {
        try {
            $token = $this->token($request);

            $payload = JWT::decode(
                $token,
                new Key($this->config->secretRefresh, $this->config->algorithm)
            );

            if (!isset($payload->data)) {
                throw new Exception('invalid session cookie');
            }

            return $schema->parse((object)$payload->data);
        } catch (Exception $e) {
            useUnauthorizedException([$e->getMessage()]);

            return $schema;
        }
    }

    public function token(Request $request): string {
        $token = $request->cookie('session');

        if (!is_string($token)) {
            useUnauthorizedException(['missing session cookie']);
        }

        return $token; // @phpstan-ignore-line
    }

    public function createSession(Response $response, array $data = []): Session {
        $refreshToken = $this->createRefreshToken($data);

        $this->createCookie(
            $response,
            $refreshToken,
            time() + $this->config->ttlRefresh
        );

        return new Session(
            $this->createShortToken($data),
            $refreshToken,
        );
    }

    public function destroySession(Response $response): void {
        $this->createCookie($response, '', time() - 86400);
    }

    public function createShortToken(array $data = []): string {
        return $this->createToken($data, $this->config->secretShort, $this->config->ttlShort);
    }

    public function createRefreshToken(array $data = []): string {
        return $this->createToken($data, $this->config->secretRefresh, $this->config->ttlRefresh);
    }

    public function createCookie(Response $response, string $token, int $expires): void {
        $domain   = parse_url($this->config->domain, PHP_URL_HOST);
        $dateTime = new DateTime();
        $format   = 'D, d M Y H:i:s e';

        $dateTime->setTimestamp($expires);
        $dateTime->setTimezone(new DateTimeZone('UTC'));
        $expire = $dateTime->format($format);

        $response->withHeader(
            'Set-Cookie',
            "session={$token}; expires={$expire}; path=/; domain={$this->config->domain}; HttpOnly"
        );
    }

    private function createToken(array $data, string $secret, int $ttl): string {
        $now = time();

        $payload = [
            'iss'  => $this->config->issuer,
            'aud'  => $this->config->audience,
            'iat'  => $now,
            'nbf'  => $now,
            'exp'  => $now + $ttl,
            'data' => $data,
        ];

        return JWT::encode($payload, $secret, $this->config->algorithm);
    }
}