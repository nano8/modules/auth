<?php

namespace laylatichy\nano\modules\auth;

final class Session {
    public function __construct(
        public readonly string $short,
        public readonly string $refresh,
    ) {}
}