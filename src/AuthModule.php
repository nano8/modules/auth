<?php

namespace laylatichy\nano\modules\auth;

use laylatichy\nano\modules\NanoModule;
use laylatichy\nano\Nano;

final class AuthModule implements NanoModule {
    public Auth $auth;

    public function __construct(
        private readonly string $secretShort,
        private readonly string $secretRefresh,
        private readonly string $issuer,
        private readonly string $audience,
        private readonly string $domain,
        private readonly string $algorithm,
        private readonly int $ttlShort,
        private readonly int $ttlRefresh,
    ) {
        // nothing to do here
    }

    public function register(Nano $nano): void {
        if (isset($this->auth)) {
            useNanoException('auth module already registered');
        }

        $this->auth = new Auth(
            new Config(
                $this->secretShort,
                $this->secretRefresh,
                $this->issuer,
                $this->audience,
                $this->domain,
                $this->algorithm,
                $this->ttlShort,
                $this->ttlRefresh,
            ),
        );
    }
}
