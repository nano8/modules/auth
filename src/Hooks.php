<?php

use laylatichy\nano\modules\auth\Auth;
use laylatichy\nano\modules\auth\AuthModule;
use laylatichy\nano\modules\auth\exceptions\ForbiddenException;
use laylatichy\nano\modules\auth\exceptions\UnauthorizedException;

if (!function_exists('useAuth')) {
    function useAuth(): Auth {
        return useNanoModule(AuthModule::class)->auth;
    }
}

// exceptions
if (!function_exists('useUnauthorizedException')) {
    /**
     * @param string[] $errors
     */
    function useUnauthorizedException(array $errors = ['unauthorized']): void {
        throw new UnauthorizedException($errors);
    }
}

if (!function_exists('useForbiddenException')) {
    /**
     * @param string[] $errors
     */
    function useForbiddenException(array $errors = ['forbidden']): void {
        throw new ForbiddenException($errors);
    }
}
