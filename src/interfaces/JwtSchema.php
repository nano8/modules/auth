<?php

namespace laylatichy\nano\modules\auth\interfaces;

use stdClass;

interface JwtSchema {
    // parse jwt data into object
    public function parse(stdClass $data): self;
}