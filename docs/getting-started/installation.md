---
layout: doc
---

## installation

you can install this module using composer

```sh
composer require laylatichy/nano-modules-auth
```

## registering the module

```php
use laylatichy\nano\modules\auth\AuthModule;

useNano()->withModule(new AuthModule(
    'secret_short',
    'secret_refresh',
    'issuer',
    'audience',
    'domain',
    'algorithm',
    'ttl_short',
    'ttl_refresh',
));
```

