---
layout: home

hero:
    name:    nano/modules/auth
    tagline: jwt authentication module for nano
    actions:
        -   theme: brand
            text:  get started
            link:  /getting-started/introduction

features:
    -   title:   simple and minimal, always
        details: |
                 nano is a simple and minimal framework, and so are its modules. nano/modules/auth is a jwt authentication module for nano, and it is simple and minimal, just like nano
    -   title:   jwt authentication
        details: |
                 nano/modules/auth provides a simple way to authenticate users using jwt tokens. it also provides a simple way to protect routes from unauthorized access
---
