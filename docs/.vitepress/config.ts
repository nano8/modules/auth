import { defineConfig } from 'vitepress';

export default defineConfig({
    lastUpdated:   true,
    sitemap:       {
        hostname: 'https://nano8.gitlab.io/modules/auth/',
    },
    title:         'nano/modules/auth',
    titleTemplate: 'nano/modules/auth - documentation',
    description:   'jwt auth module for laylatichy/nano framework',
    base:          '/modules/auth/',
    themeConfig:   {
        lastUpdated: {
            text:          'updated',
            formatOptions: {
                dateStyle: 'long',
                timeStyle: 'medium',
            },
        },
        search:      {
            provider: 'local',
        },
        nav:         [],
        sidebar:     [
            {
                text:  'getting started',
                items: [
                    {
                        text: 'introduction',
                        link: '/getting-started/introduction',
                    },
                    {
                        text: 'installation',
                        link: '/getting-started/installation',
                    },
                ],
            },
            {
                text:  'usage',
                items: [
                    {
                        text: 'useAuth',
                        link: '/usage/use-auth',
                    },
                    {
                        text: 'useUnauthorizedException',
                        link: '/usage/use-unauthorized-exception',
                    },
                    {
                        text: 'useForbiddenException',
                        link: '/usage/use-forbidden-exception',
                    },
                ],
            },
            {
                text: 'nano documentation',
                link: 'https://nano.laylatichy.com',
            },
        ],
        socialLinks: [
            {
                icon: 'github',
                link: 'https://gitlab.com/nano8/modules/auth',
            },
        ],
        editLink:    {
            text:    'edit this page',
            pattern: 'https://gitlab.com/nano8/modules/auth/-/edit/dev/docs/:path',
        },
    },
});
