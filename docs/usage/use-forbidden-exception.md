---
layout: doc
---

<script setup>
const args = {
    useForbiddenException: [
        { name: 'errors', type: 'string[]' },
    ],
};
</script>

## usage

`useForbiddenException` is a helper function that returns the `ForbiddenException` response

## <Types fn="useForbiddenException" r="void" :args="args.useForbiddenException" /> {#useForbiddenException}

```php
useForbiddenException(['you are not allowed to do this', 'you are not allowed to do that']);
```

will generate the following response and status code 403

```json
{
    "code":     403,
    "response": [
        "you are not allowed to do this",
        "you are not allowed to do that"
    ]
}
```

