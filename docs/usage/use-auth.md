---
layout: doc
---

<script setup>
const args = {
    useAuth: [],
    short: [
        { name: 'request', type: 'Request' },
        { name: 'schema', type: 'JwtSchema' },
    ],
    refresh: [
        { name: 'request', type: 'Request' },
        { name: 'schema', type: 'JwtSchema' },
    ],
    token: [
        { name: 'request', type: 'Request' },
    ],
    createSession: [
        { name: 'response', type: 'Response' },
        { name: 'data', type: 'array' },
    ],
    destroySession: [
        { name: 'response', type: 'Response' },
    ],
    createShortToken: [
        { name: 'data', type: 'array' },
    ],
    createRefreshToken: [
        { name: 'data', type: 'array' },
    ],
    createToken: [
        { name: 'data', type: 'array' },
        { name: 'secret', type: 'string' },
        { name: 'ttl', type: 'int' },
    ],
    createCookie: [
        { name: 'response', type: 'Response' },
        { name: 'token', type: 'string' },
        { name: 'expires', type: 'int' },
    ],
};
</script>

##### laylatichy\nano\modules\auth\Auth

## usage

`useAuth` is a helper function that returns the `auth` module

## <Types fn="useAuth" r="Auth" :args="args.useAuth" /> {#useAuth}

```php
useAuth();
```

## <Types fn="short" r="JwtSchema" :args="args.short" /> {#short}

returns parsed jwt data from request using short-lived token from Authorization Bearer header

```php
useAuth()->short($request, $schema);
```

## <Types fn="refresh" r="JwtSchema" :args="args.refresh" /> {#refresh}

returns parsed jwt data from request using refresh token from the session cookie

```php
useAuth()->refresh($request, $schema);
```

## <Types fn="token" r="string" :args="args.token" /> {#token}

get raw refresh token from the session cookie

```php
useAuth()->token($request);
```

## <Types fn="createSession" r="Session" :args="args.createSession" /> {#createSession}

create a session cookie, short-lived and refresh tokens

```php
useAuth()->createSession($response, []);
```

## <Types fn="destroySession" r="void" :args="args.destroySession" /> {#destroySession}

destroy the session cookie

```php
useAuth()->destroySession($response);
```

## <Types fn="createShortToken" r="string" :args="args.createShortToken" /> {#createShortToken}

create a short-lived token

```php
useAuth()->createShortToken([]);
```

## <Types fn="createRefreshToken" r="string" :args="args.createRefreshToken" /> {#createRefreshToken}

create a refresh token

```php
useAuth()->createRefreshToken([]);
```

## <Types fn="createToken" r="string" :args="args.createToken" /> {#createToken}

create a token with a custom ttl and secret

```php
useAuth()->createToken([], 'secret', 3600);
```

## <Types fn="createCookie" r="void" :args="args.createCookie" /> {#createCookie}

create a session cookie

```php
useAuth()->createCookie($response, 'token', time() + 3600);
```

