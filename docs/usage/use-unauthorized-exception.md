---
layout: doc
---

<script setup>
const args = {
    useUnauthorizedException: [
        { name: 'errors', type: 'string[]' },
    ],
};
</script>

## usage

`useUnauthorizedException` is a helper function that returns the `UnauthorizedException` response

## <Types fn="useUnauthorizedException" r="void" :args="args.useUnauthorizedException" /> {#useUnauthorizedException}

```php
useUnauthorizedException(['you are not allowed to do this', 'you are not allowed to do that']);
```

will generate the following response and status code 401

```json
{
    "code":     401,
    "response": [
        "you are not allowed to do this",
        "you are not allowed to do that"
    ]
}
```

